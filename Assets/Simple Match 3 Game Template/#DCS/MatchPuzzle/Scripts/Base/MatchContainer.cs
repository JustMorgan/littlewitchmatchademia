﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;// for matchPoint view
using System.Collections.Generic;// for List<>
using UnityEngine.EventSystems;// for using drag
using DCS.MatchPuzzle.Base;// for Enum and Constants and Structs

public class MatchContainer : MonoBehaviour
{
	#region Custom Part

	#region Public Field

	/// <summary>
	/// MatchContainer 의 가로줄 갯수
	/// 
	/// the number of row line
	/// </summary>
	public int rowSize;

	/// <summary>
	/// MatchContainer 의 세로줄 갯수
	/// 
	/// the number of column line
	/// </summary>
	public int columnSize;

	/// <summary>
	/// Block 의 Prefab 오브젝트
	/// 
	/// prefab object of the Block
	/// </summary>
	public GameObject blockPrefab;

	/// <summary>
	/// Block 의 가로세로 크기 (단위 : 픽셀)
	/// 
	/// size of the Block (unit : pixel)
	/// </summary>
	public int blockSize = 35;

	/// <summary>
	/// Block 사이의 거리 (단위 : 픽셀)
	/// 
	/// distance between Block (unit : pixel)
	/// </summary>
	public int blockSpacing = 2;

	/// <summary>
	/// Item 사용 여부
	/// 
	/// when you want to use item. set true
	/// </summary>
	public bool useItems = true;

	/// <summary>
	/// 스코어 가 표기될 라벨
	/// 
	/// show score label
	/// </summary>
	public Text scoreLabel;

	/// <summary>
	/// 콤보 가 표기될 라벨
	/// 
	/// show combo label
	/// </summary>
	public Text comboLabel;

	#endregion

	#region Effect

	public EffectPool effectPool;

	/// <summary>
	/// Index 를 받아 해당 위치에 가로줄 파괴 이펙트를 실행함
	/// 
	/// executing the 'Horizontal Line Destroy Effect' with the 'Index'
	/// </summary>
	public void EffectItemHorizontal(Index index)
	{
		Vector2 temp = IndexRowToPosition(index.row);
		effectPool.UseItemHorizontal(new Vector3(temp.x, temp.y, -50f));
	}

	/// <summary>
	/// Index 를 받아 해당 위치에 가로줄 3개 파괴 이펙트를 실행함
	/// 
	/// executing the 'Horizontal Line * 3 Destroy Effect' with the 'Index'
	/// </summary>
	public void EffectItemHorizontal3(Index index)
	{
		Vector2 temp = IndexRowToPosition(index.row);
		effectPool.UseItemHorizontal3(new Vector3(temp.x, temp.y, -50f));
	}

	/// <summary>
	/// Index 를 받아 해당 위치에 세로줄 파괴 이펙트를 실행함
	/// 
	/// executing the 'Vertical Line Destroy Effect' with the 'Index'
	/// </summary>
	public void EffectItemVertical(Index index)
	{
		Vector2 temp = IndexColumnToPosition(index.column);
		effectPool.UseItemVertical(new Vector3(temp.x, temp.y, -50f));
	}

	/// <summary>
	/// Index 를 받아 해당 위치에 세로줄 3개 파괴 이펙트를 실행함
	/// 
	/// executing the 'Vertical Line * 3 Destroy Effect' with the 'Index'
	/// </summary>
	public void EffectItemVertical3(Index index)
	{
		Vector2 temp = IndexColumnToPosition(index.column);
		effectPool.UseItemVertical3(new Vector3(temp.x, temp.y, -50f));
	}

	/// <summary>
	/// Index 를 받아 해당 위치에 Box 형태로 파괴 이펙트를 실행함
	/// 
	/// executing the 'Box Shape Destroy Effect' with the 'Index'
	/// </summary>
	public void EffectItemBox(Index index)
	{
		Vector3 temp = IndexToPosition(index);
		effectPool.UseItemBox(new Vector3(temp.x, temp.y, -50f));
	}

	/// <summary>
	/// Index 를 받아 해당 위치에 Box + Box 형태로 파괴 이펙트를 실행함
	/// 
	/// executing the 'Big Box Shape Destroy Effect' with the 'Index'
	/// </summary>
	public void EffectItemBoxAndBox(Index index)
	{
		Vector3 temp = IndexToPosition(index);
		effectPool.UseItemBoxAndBox(new Vector3(temp.x, temp.y, -50f));
	}

	/// <summary>
	/// Index 와 activeTime 을 받아
	/// activeTime 만큼 해당 위치에 지속되는 Ultimate 파괴 이펙트를 실행함
	/// 
	/// executing the 'Same BlockType Destroy Effect' with the 'Index' And 'activeTime'
	/// </summary>
	public void EffectItemUltimate(Index index, float activeTime)
	{
		Vector3 temp = IndexToPosition(index);
		effectPool.UseItemUltimate(new Vector3(temp.x, temp.y, -50f), activeTime);
	}

	/// <summary>
	/// from Index 와 to Index 를 받아
	/// from 에서 to 까지 이어지는 Ultimate 파괴 이펙트를 실행함
	/// 
	/// executing the 'Same BlockType Destroy Effect (Line)' with the 'from Index' And 'to Index'
	/// </summary>
	public void EffectItemUltimateLine(Index from, Index to)
	{
		Vector3 a = IndexToPosition(from);
		Vector3 b = IndexToPosition(to);
		effectPool.UseItemUltimateLine(new Vector3(a.x, a.y, -50f), new Vector3(b.x, b.y, -50f));
	}

	#endregion
	#region Sound

	public SoundController soundController;

	/// <summary>
	/// 일반적 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyNormal()
	{
		soundController.PlayBeepDown();
	}

	/// <summary>
	/// 가로 아이템 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyHorizontal()
	{
		soundController.PlayCrack();
	}

	/// <summary>
	/// 세로 아이템 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyVertical()
	{
		soundController.PlayCrack();
	}

	/// <summary>
	/// 박스 아이템 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyBox()
	{
		soundController.PlayCrack();
	}

	/// <summary>
	/// 궁극 아이템 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyUltimate()
	{
		soundController.PlayBeep();
	}

	/// <summary>
	/// 가로세로 아이템 + 가로세로 아이템 (으)로 파괴시 실행될 사운드 셋팅팅
	/// </summary>
	public void PlaySoundDestroyBoth()
	{
		soundController.PlayCrack();
	}

	/// <summary>
	/// 가로세로 아이템 + 박스 아이템 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyBoxAndBoth()
	{
		soundController.PlayCrack();
	}

	/// <summary>
	/// 박스 아이템 + 박스 아이템 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyBoxAndBox()
	{
		soundController.PlayCrack();
	}

	/// <summary>
	/// 궁극 아이템 + 가로세로 아이템 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyUltimateAndBoth()
	{
		soundController.PlayBeep();
	}

	/// <summary>
	/// 궁극 아이템 + 박스 아이템 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyUltimateAndBox()
	{
		soundController.PlayBeep();
	}

	/// <summary>
	/// 궁극 아이템 + 궁극 아이템 (으)로 파괴시 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundDestroyUltimateAndUltimate()
	{
		soundController.PlayBeep();
	}

	/// <summary>
	/// 파괴 후 아이템으로 변할 때 실행될 사운드 셋팅
	/// </summary>
	public void PlaySoundChangeToItemBlock()
	{
		soundController.PlayDribble();
	}

	#endregion
	#region Combo

	/// <summary>
	/// 콤보 표기 방식 셋팅
	/// </summary>
	void SetCombo(int combo)
	{
		if (comboLabel == null) return;

		this.combo = combo;
		if (combo != 0)
		{
			LeanTween.textAlpha(comboLabel.rectTransform, 1f, 0.3f).setFrom(0f).setEase(LeanTweenType.easeOutExpo);
			LeanTween.scale(comboLabel.gameObject, Vector2.one, 0.3f).setFrom(new Vector2(1.4f, 1.4f)).setEase(LeanTweenType.easeOutExpo);
			comboLabel.text = "COMBO " + this.combo.ToString() + "!";
		}
		else
		{
			LeanTween.textAlpha(comboLabel.rectTransform, 0f, 0.3f).setFrom(1f).setEase(LeanTweenType.easeInOutSine);
		}
	}

	#endregion
	
	#region TEST

	/// <summary>
	/// Start And Reset 버튼에서 사용하는 함수
	/// 
	/// Just use 'Start And Reset' Button
	/// </summary>
	public void TEST_ContainerReset(RectTransform objRect)
	{
		StartCoroutine(TEST_ContainerReset_C(objRect));
	}
	IEnumerator TEST_ContainerReset_C(RectTransform objRect)
	{
		ResetGame();

		soundController.PlayStartButton();

		RectTransform textRect = objRect.Find("Text").GetComponent<RectTransform>();

		LeanTween.color(objRect, Color.white, .75f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(objRect, 0f, .75f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.textColor(textRect, Color.black, .75f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.textAlpha(textRect, 0f, .75f).setEase(LeanTweenType.easeInOutSine);

		yield return new WaitForSeconds(.75f);

		objRect.gameObject.SetActive(false);
	}

	#endregion

	#endregion

	#region Algorithm Part

	#region Enum

	public enum State : int
	{
		On,// It is Possible. Drag and block destroy
		Off,// Is it Not Possible. Anything
		Stop,// Is it Not Possible. Drag and block destroy and block move
	}

	#endregion

	#region Public Field

	public Block[,] blocks;

	

	#endregion

	#region Private Field

	State mState = State.Off;

	RectTransform mRect;

	List<Block> matchList = new List<Block>();

	Vector2 revisePosition;
	double sidePad;
	int blockBeta;

	Block firstBlock, secondBlock;

	int preScore;
	int curScore;

	int combo;
	float comboCheckTime = 0f;
	bool comboCheck = false;

	bool isMatchDone;

	#endregion

	#region Interfaces
	/**
	public void OnBeginDrag(PointerEventData eventData)
	{
		if (!eventData.pointerEnter || eventData.pointerEnter.tag != "Block") return;

		Block select = eventData.pointerEnter.GetComponent<Block>();
		Block.State sState = select.state;

		if (sState == Block.State.Action) { firstBlock = null; return; }

		firstBlock = select;
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (!firstBlock) return;

		Vector2 delta = eventData.delta.normalized;

		int r = firstBlock.index.row;
		int c = firstBlock.index.column;

		if (!secondBlock)
		{
			if (delta.y > dragSensitive)// drag up
				if (r - 1 >= 0)
				{
					secondBlock = blocks[r - 1, c];
					DragComplete(); return;
				}
			if (delta.y < -dragSensitive)// drag down
				if (r + 1 < rowSize)
				{
					secondBlock = blocks[r + 1, c];
					DragComplete(); return;
				}
			if (delta.x < -dragSensitive)// drag left
				if (c - 1 >= 0)
				{
					secondBlock = blocks[r, c - 1];
					DragComplete(); return;
				}
			if (delta.x > dragSensitive)// drag right
				if (c + 1 < columnSize)
				{
					secondBlock = blocks[r, c + 1];
					DragComplete(); return;
				}
		}
	}
	float dragSensitive = 0.5f;

	public void OnEndDrag(PointerEventData eventData)
	{
		DragComplete();
	}

	*/
	#endregion

	#region Default Functions

	void Start()
	{
		blocks = new Block[rowSize, columnSize];
		mRect = GetComponent<RectTransform>();
		revisePosition = new Vector2(-mRect.rect.width * 0.5f, mRect.rect.height);
		sidePad = (mRect.rect.width - (blockSize * rowSize + blockSpacing * (rowSize - 1))) * 0.5 + blockSize * 0.5;
		blockBeta = blockSize + blockSpacing;

		InitBlocks();
	}

	void Update()
	{
		if (comboCheck && isMatchDone) comboCheckTime += Time.deltaTime;
		if (combo != 0 && comboCheckTime > Constants.comboCheckMaxTime) { SetCombo(0); comboCheck = false; }

		if (preScore != curScore && !isScoreChange && scoreLabel != null) { StartCoroutine(ScoreChange_C(preScore, curScore)); preScore = curScore; }
	}

	IEnumerator UpdateBlockEmptyCheck_C()
	{
		BlocksPositionDown();

		while (mState == State.Stop) { yield return new WaitForSeconds(0.02f); }// 멈춤 상태일땐 대기

		yield return null;
		StartCoroutine(UpdateBlockEmptyCheck_C());
	}

	#endregion

	#region Public Functions

	public void InitBlocks()
	{
		if (blockPrefab == null) { Debug.Log("blockPrefab field is Null!"); return; }

		for (int r = 0; r < rowSize; r++)
		{
			for (int c = 0; c < columnSize; c++)
			{
				if (blocks[r, c] == null)
				{
					GameObject obj = Instantiate(blockPrefab) as GameObject;
					obj.transform.SetParent(transform, false);
					obj.transform.localPosition = new Vector3(-100f, -100f, 0f);
					obj.transform.localScale = Vector2.one;
					obj.tag = "Block";
					obj.name = r.ToString() + " " + c.ToString();

					Block block = obj.GetComponent<Block>();
					blocks[r, c] = block;
				}
			}
		}
	}

	public void ResetGame()
	{
		curScore = 0;
		ResetAllBlockPosition();
		ResetAllBlockTypeRandom_R();
		ResetAllBlockItemType();
		ResetAllBlockState();
		ContainerOn();
		AllBlockOn();
	}

	public void ResetAllBlockCannotMatch()
	{
		StartCoroutine(ResetAllBlockCannotMatch_C());
	}
	IEnumerator ResetAllBlockCannotMatch_C()
	{
		mState = State.Stop;
		ResetAllBlockPosition();

		AllBlockOff();
		yield return new WaitForSeconds(Constants.blocksOnDuration);

		ResetAllBlockTypeRandom_R();
		AllBlockOn();
		mState = State.On;
	}

	public void ResetAllBlockPosition()
	{
		for (int r = 0; r < rowSize; r++)
			for (int c = 0; c < columnSize; c++)
			{
				blocks[r, c].SetIndexAndPosition(r, c);
			}
	}

	public void ResetAllBlockTypeRandom_R()
	{
		for (int r = 0; r < rowSize; r++)
			for (int c = 0; c < columnSize; c++)
			{
				blocks[r, c].SetRandomType();
				while (CheckMatchForReset(blocks[r, c])) blocks[r, c].SetRandomType();
			}

		if (!checkMatchPossible()) ResetAllBlockTypeRandom_R();
	}
	bool CheckMatchForReset(Block block)
	{
		int r = block.index.row;
		int c = block.index.column;
		BlockType type = block.type;

		// left
		if (c >= 2 && type == blocks[r, c - 1].type && type == blocks[r, c - 2].type) return true;
		// right
		else if (c < columnSize - 2 && type == blocks[r, c + 1].type && type == blocks[r, c + 2].type) return true;
		// top
		else if (r >= 2 && type == blocks[r - 1, c].type && type == blocks[r - 2, c].type) return true;
		// bottom
		else if (r < rowSize - 2 && type == blocks[r + 1, c].type && type == blocks[r + 2, c].type) return true;

		return false;
	}

	public void ResetAllBlockItemType()
	{
		for (int r = 0; r < rowSize; r++)
			for (int c = 0; c < columnSize; c++)
			{
				blocks[r, c].SetItem(ItemType.None);
			}
	}

	public void ResetAllBlockState()
	{
		for (int r = 0; r < rowSize; r++)
			for (int c = 0; c < columnSize; c++)
			{
				blocks[r, c].SetState(Block.State.Idle);
			}
	}

	public void AllBlockOn()
	{
		for(int r = 0 ; r < rowSize ; r++)
			for (int c = 0; c < columnSize; c++)
			{
				blocks[r, c].SetOn();
			}
	}

	public void AllBlockOff()
	{
		for (int r = 0; r < rowSize; r++)
			for (int c = 0; c < columnSize; c++)
			{
				blocks[r, c].SetOff();
			}
	}

	public void ContainerOn()
	{
		if (mState == State.On) return;
		mState = State.On;
		StartCoroutine(UpdateBlockEmptyCheck_C());
	}

	public void ContainerStop(bool stopCondition)
	{
		if (stopCondition)
		{
			mState = State.Stop;
			containerStopCount++;
		}
		else
		{
			containerStopCount--;
			if (containerStopCount == 0)
			{
				mState = State.On;
			}
		}
	}
	int containerStopCount = 0;

	public void ContainerOff()
	{
		mState = State.Off;
		StopAllCoroutines();
	}

	public void DestroyBlockList(List<Block> blockList)
	{
		for (int i = 0; i < blockList.Count; i++) blockList[i].Destroy();

		blockList.Clear();
	}

	public void BlocksPositionDown()
	{
		for (int c = columnSize - 1; c >= 0; c--)
		{
			List<Block> emptyBlockList = GetBlocksEmpty(c);
			if (emptyBlockList.Count != 0)
			{
				for (int i = 0; i < emptyBlockList.Count; i++)
				{
					emptyBlockList[i].SetPosition(-1 - i, c);
					emptyBlockList[i].SetRandomType();
				}

				for (int r = rowSize - 1; r >= 0; r--)
					BlockIndexDown_R(blocks[r, c]);

				for (int r = 0; r < rowSize; r++)
					blocks[r, c].Move(new Index(r, c));
			}
		}
	}

	public void AddScore(int basePoint)
	{
		int temp = Mathf.RoundToInt(basePoint + basePoint * ((combo - 1) * Constants.comboFactor));
		curScore += temp;
	}

	public void AddCombo()
	{
		comboCheckTime = 0f;
		comboCheck = true;
		int result = combo + 1;
		SetCombo(result);
	}

	public List<Block> GetBlocksHorizontal(Block block)
	{
		List<Block> newBlocks = new List<Block>();
		for (int c = 0; c < this.columnSize; c++)
		{
			newBlocks.Add(blocks[block.index.row, c]);
		}
		return newBlocks;
	}
	public List<Block> GetBlocksHorizontal(Block block, int size)
	{
		List<Block> newBlocks = new List<Block>();

		int r = block.index.row;
		int c = block.index.column;

		int half = size / 2;

		for (int i = -half; i < size - half; i++)
		{
			if (r + i >= 0 && r + i < this.rowSize)
			{
				List<Block> temp = GetBlocksHorizontal(blocks[r + i, c]);
				for (int t = 0; t < temp.Count; t++) newBlocks.Add(temp[t]);
			}
		}

		return newBlocks;
	}

	public List<Block> GetBlocksVertical(Block block)
	{
		List<Block> newBlocks = new List<Block>();
		for (int r = 0; r < this.rowSize; r++)
		{
			newBlocks.Add(blocks[r, block.index.column]);
		}
		return newBlocks;
	}
	public List<Block> GetBlocksVertical(Block block, int size)
	{
		List<Block> newBlocks = new List<Block>();

		int r = block.index.row;
		int c = block.index.column;

		int half = size / 2;

		for (int i = -half; i < size - half; i++)
		{
			if (c + i >= 0 && c + i < this.columnSize)
			{
				List<Block> temp = GetBlocksVertical(blocks[r, c + i]);
				for (int t = 0; t < temp.Count; t++) newBlocks.Add(temp[t]);
			}
		}

		return newBlocks;
	}

	public List<Block> GetBlocksVerticalAndHorizontal(Block block)
	{
		List<Block> newBlocks = new List<Block>();
		for (int r = 0; r < this.rowSize; r++)
		{
			newBlocks.Add(blocks[r, block.index.column]);
		}
		for (int c = 0; c < this.columnSize; c++)
		{
			if (block.index.column != c) newBlocks.Add(blocks[block.index.row, c]);
		}
		return newBlocks;
	}

	public List<Block> GetBlocksBox(Block block, int size)
	{
		List<Block> newBlocks = new List<Block>();

		int c = block.index.column;
		int r = block.index.row;

		int half = size / 2;

		for (int ri = -half; ri < size - half; ri++)
		{
			for (int ci = -half; ci < size - half; ci++)
			{
				int tempC = c + ci;
				int tempR = r + ri;
				if (tempC >= 0 && tempC < this.columnSize && tempR >= 0 && tempR < this.rowSize) newBlocks.Add(blocks[tempR, tempC]);
			}
		}

		return newBlocks;
	}

	public List<Block> GetBlocksUltimate(BlockType type)
	{
		List<Block> newBlocks = new List<Block>();
		foreach (Block b in blocks)
		{
			if (b.type == type)
			{
				newBlocks.Add(b);
			}
		}
		return newBlocks;
	}

	public List<Block> GetBlocksEmpty(int column)
	{
		List<Block> temp = new List<Block>();
		for (int r = 0; r < rowSize; r++)
			if (blocks[r, column].state == Block.State.Empty) temp.Add(blocks[r, column]);
		return temp;
	}

	#endregion

	#region Private Functions

	void BlockSwap(int aRow, int aColumn, int bRow, int bColumn)
	{
		Block temp = blocks[aRow, aColumn];
		blocks[aRow, aColumn] = blocks[bRow, bColumn];
		blocks[bRow, bColumn] = temp;

		blocks[aRow, aColumn].SetIndex(aRow, aColumn);
		blocks[bRow, bColumn].SetIndex(bRow, bColumn);
	}

	void BlockIndexDown_R(Block block)
	{
		if (block.index.row + 1 < rowSize)
		{
			if (blocks[block.index.row + 1, block.index.column].state == Block.State.Empty)
			{
				Index aIndex = block.index;
				Index bIndex = blocks[block.index.row + 1, block.index.column].index;

				BlockSwap(aIndex.row, aIndex.column, bIndex.row, bIndex.column);
				BlockIndexDown_R(block);
			}
		}
	}

	void DragComplete()
	{
		if (mState == State.Stop) return;

		StartCoroutine(DragComplete_C());
		firstBlock = null;
		secondBlock = null;
	}
	IEnumerator DragComplete_C()
	{
		if (!firstBlock || !secondBlock) yield break;

		if (!CheckDragAble()) yield break;

		Block a = firstBlock;
		Block b = secondBlock;

		yield return StartCoroutine(BlockSwap_C(a, b));

		bool aMatch = isMatch(a);
		bool bMatch = isMatch(b);
		ItemCombineType iMatch = isMatch_ItemCombine(a, b);

		if (iMatch != ItemCombineType.None)
		{
			isMatchDone = false;

			AddCombo();
			
			a.SetState(Block.State.Action);
			b.SetState(Block.State.Action);

			switch (iMatch)
			{
				case ItemCombineType.Both:
					{
						yield return StartCoroutine(DestroyBlockListForBoth(a));
					} break;
				case ItemCombineType.BoxAndBoth:
					{
						yield return StartCoroutine(DestroyBlockListForBoxAndBoth(a));
					} break;
				case ItemCombineType.BoxAndBox:
					{
						yield return StartCoroutine(DestroyBlockListForBoxAndBox(a));
					} break;
				case ItemCombineType.Ultimate:
					{
						yield return StartCoroutine(DestroyBlockListForUltimate(a, b));
					} break;
				case ItemCombineType.UltimateAndBoth:
					{
						yield return StartCoroutine(DestroyBlockListForUltimateAndBoth(a, b));
					} break;
				case ItemCombineType.UltimateAndBox:
					{
						yield return StartCoroutine(DestroyBlockListForUltimateAndBox(a, b));
					} break;
				case ItemCombineType.UltimateAndUltimate:
					{
						yield return StartCoroutine(DestroyBlockListForUltimateAndUltimate(a, b));
					} break;
			}

			LeanTween.scale(a.gameObject, Vector2.zero, Constants.destroyNormalDuration).setFrom(Vector2.one).setEase(LeanTweenType.easeInOutSine);
			LeanTween.scale(b.gameObject, Vector2.zero, Constants.destroyNormalDuration).setFrom(Vector2.one).setEase(LeanTweenType.easeInOutSine);
			a.SetState(Block.State.Empty);
			b.SetState(Block.State.Empty);
			a.SetItem(ItemType.None);
			b.SetItem(ItemType.None);
		}
		else if (aMatch || bMatch)// normal match
		{
			isMatchDone = false;

			AddCombo();

			DestroyBlockList(matchList);

			PlaySoundDestroyNormal();
		}
		else
		{
			yield return StartCoroutine(BlockSwap_C(a, b));
		}
	}
	IEnumerator BlockSwap_C(Block a, Block b)
	{
		a.SetState(Block.State.Action);
		b.SetState(Block.State.Action);

		Vector2 aPos = IndexToPosition(a.index);
		Vector2 bPos = IndexToPosition(b.index);

		LeanTween.moveLocal(a.gameObject, bPos, Constants.swapDuration).setFrom(aPos).setEase(LeanTweenType.easeOutSine);
		LeanTween.moveLocal(b.gameObject, aPos, Constants.swapDuration).setFrom(bPos).setEase(LeanTweenType.easeOutSine);
		yield return new WaitForSeconds(Constants.swapDuration);

		a.transform.localPosition = bPos;
		b.transform.localPosition = aPos;

		BlockSwap(a.index.row, a.index.column, b.index.row, b.index.column);

		a.SetState(Block.State.Idle);
		b.SetState(Block.State.Idle);
	}

	bool CheckDragAble()
	{
		for (int r = 0; r < this.rowSize; r++)
		{
			for (int c = 0; c < this.columnSize; c++)
			{
				if (blocks[r, c].state != Block.State.Idle) return false;
			}
		}
		return true;
	}

	bool isMatch(Block block)
	{
		if (block.state == Block.State.Action) return false;

		int r = block.index.row;
		int c = block.index.column;

		int uc = countMatchUp(r, c);
		int dc = countMatchDown(r, c);
		int lc = countMatchLeft(r, c);
		int rc = countMatchRight(r, c);
		int vertical = 1 + uc + dc;
		int horizontal = 1 + lc + rc;

		if (vertical >= 3 || horizontal >= 3)
		{
			SetMatchList(block);
			return true;
		}
		return false;
	}

	ItemCombineType isMatch_ItemCombine(Block a, Block b)
	{
		if (a.item == ItemType.None && b.item == ItemType.None) return ItemCombineType.None;

		// Horizontal, Vertical & Horizontal, Vertical
		if (a.item == ItemType.Horizontal && b.item == ItemType.Horizontal)
		{
			return ItemCombineType.Both;
		}
		else if (a.item == ItemType.Horizontal && b.item == ItemType.Vertical)
		{
			return ItemCombineType.Both;
		}
		else if (a.item == ItemType.Vertical && b.item == ItemType.Vertical)
		{
			return ItemCombineType.Both;
		}
		else if (a.item == ItemType.Vertical && b.item == ItemType.Horizontal)
		{
			return ItemCombineType.Both;
		}

		// Horizontal, Vertical & Box
		else if (a.item == ItemType.Horizontal && b.item == ItemType.Box)
		{
			return ItemCombineType.BoxAndBoth;
		}
		else if (a.item == ItemType.Box && b.item == ItemType.Horizontal)
		{
			return ItemCombineType.BoxAndBoth;
		}
		else if (a.item == ItemType.Vertical && b.item == ItemType.Box)
		{
			return ItemCombineType.BoxAndBoth;
		}
		else if (a.item == ItemType.Box && b.item == ItemType.Vertical)
		{
			return ItemCombineType.BoxAndBoth;
		}

		// Box & Box
		else if (a.item == ItemType.Box && b.item == ItemType.Box)
		{
			return ItemCombineType.BoxAndBox;
		}

		// Ultimate & Horizontal
		else if (a.item == ItemType.Ultimate && b.item == ItemType.Horizontal)
		{
			return ItemCombineType.UltimateAndBoth;
		}
		else if (a.item == ItemType.Horizontal && b.item == ItemType.Ultimate)
		{
			return ItemCombineType.UltimateAndBoth;
		}

		// Ultimate & Vertical
		else if (a.item == ItemType.Ultimate && b.item == ItemType.Vertical)
		{
			return ItemCombineType.UltimateAndBoth;
		}
		else if (a.item == ItemType.Vertical && b.item == ItemType.Ultimate)
		{
			return ItemCombineType.UltimateAndBoth;
		}

		// Ultimate & Box
		else if (a.item == ItemType.Ultimate && b.item == ItemType.Box)
		{
			return ItemCombineType.UltimateAndBox;
		}
		else if (a.item == ItemType.Box && b.item == ItemType.Ultimate)
		{
			return ItemCombineType.UltimateAndBox;
		}

		// Ultimate & Ultimate
		else if (a.item == ItemType.Ultimate && b.item == ItemType.Ultimate)
		{
			return ItemCombineType.UltimateAndUltimate;
		}

		// Ultimate & None
		else if (a.item == ItemType.Ultimate && b.item == ItemType.None)
		{
			return ItemCombineType.Ultimate;
		}
		else if (a.item == ItemType.None && b.item == ItemType.Ultimate)
		{
			return ItemCombineType.Ultimate;
		}

		return ItemCombineType.None;
	}

	void SetMatchList(Block block)
	{
		checkList.Clear();
		settedItemType = ItemType.None;

		SetMatchList_R(block);
	}
	void SetMatchList_R(Block block)
	{
		checkList.Add(block);
		int r = block.index.row; int c = block.index.column; BlockType type = block.type;

		// block Add MatchList
		if (!matchList.Contains(block) && block.state != Block.State.Action && block.item != ItemType.Ultimate)
		{
			MatchType mType = GetMatchType(r, c, type);
			
			switch (mType)
			{
				case MatchType.None: { } break;
				case MatchType.Match3Horizontal: { block.SetAfterDestroyItem(ItemType.None); matchList.Add(block); } break;
				case MatchType.Match3Vertical: { block.SetAfterDestroyItem(ItemType.None); matchList.Add(block); } break;
				case MatchType.Match4Horizontal:
					{
						if (settedItemType < ItemType.Vertical && useItems)
						{
							block.SetAfterDestroyItem(ItemType.Vertical);
							settedItemType = ItemType.Vertical;
						}
						matchList.Add(block);
					} break;
				case MatchType.Match4Vertical:
					{
						if (settedItemType < ItemType.Horizontal && useItems)
						{
							block.SetAfterDestroyItem(ItemType.Horizontal);
							settedItemType = ItemType.Horizontal;
						}
						matchList.Add(block);
					} break;
				case MatchType.MatchBoth:
					{
						if (settedItemType < ItemType.Box && useItems)
						{
							block.SetAfterDestroyItem(ItemType.Box);
							settedItemType = ItemType.Box;
						}
						matchList.Add(block);
					} break;
				case MatchType.Match5:
					{
						if (settedItemType < ItemType.Ultimate && useItems)
						{
							block.SetAfterDestroyItem(ItemType.Ultimate);
							settedItemType = ItemType.Ultimate;
						}
						matchList.Add(block);
					} break;
			}
		}

		if (isMatchUp(r, c, type) && !isCheckedUp(r, c)) SetMatchList_R(blocks[r - 1, c]);
		if (isMatchDown(r, c, type) && !isCheckedDown(r, c)) SetMatchList_R(blocks[r + 1, c]);
		if (isMatchLeft(r, c, type) && !isCheckedLeft(r, c)) SetMatchList_R(blocks[r, c - 1]);
		if (isMatchRight(r, c, type) && !isCheckedRight(r, c)) SetMatchList_R(blocks[r, c + 1]);
	}
	List<Block> checkList = new List<Block>();
	ItemType settedItemType = ItemType.None;

	MatchType GetMatchType(int row, int column, BlockType type)
	{
		int mMatch = Constants.minimumMatch;

		int uc = countMatchUp(row, column);
		int dc = countMatchDown(row, column);
		int lc = countMatchLeft(row, column);
		int rc = countMatchRight(row, column);
		int vertical = 1 + uc + dc;// vertical match count
		int horizontal = 1 + lc + rc;// horizontal match count

		if (vertical >= mMatch && horizontal >= mMatch)
		{
			if (vertical >= 5 || horizontal >= 5) { return MatchType.Match5; }
			else { return MatchType.MatchBoth; }
		}
		else if (vertical < mMatch && horizontal >= mMatch)
		{
			if (horizontal >= 5) { return MatchType.Match5; }
			else if (horizontal == 4) { return MatchType.Match4Horizontal; }
			else { return MatchType.Match3Horizontal; }
		}
		else if (vertical >= mMatch && horizontal < mMatch)
		{
			if (vertical >= 5) { return MatchType.Match5; }
			else if (vertical == 4) { return MatchType.Match4Vertical; }
			else { return MatchType.Match3Vertical; }
		}

		return MatchType.None;
	}

	bool isMatchUp(int row, int column, BlockType type)
	{
		if (row > 0) if (type == blocks[row - 1, column].type) return true;
		return false;
	}
	bool isMatchDown(int row, int column, BlockType type)
	{
		if (row < rowSize - 1) if (type == blocks[row + 1, column].type) return true;
		return false;
	}
	bool isMatchLeft(int row, int column, BlockType type)
	{
		if (column > 0) if (type == blocks[row, column - 1].type) return true;
		return false;
	}
	bool isMatchRight(int row, int column, BlockType type)
	{
		if (column < columnSize - 1) if (type == blocks[row, column + 1].type) return true;
		return false;
	}

	bool isCheckedUp(int row, int column)
	{
		if (row > 0) if (checkList.Contains(blocks[row - 1, column])) return true;
		return false;
	}
	bool isCheckedDown(int row, int column)
	{
		if (row < rowSize - 1) if (checkList.Contains(blocks[row + 1, column])) return true;
		return false;
	}
	bool isCheckedLeft(int row, int column)
	{
		if (column > 0) if (checkList.Contains(blocks[row, column - 1])) return true;
		return false;
	}
	bool isCheckedRight(int row, int column)
	{
		if (column < columnSize - 1) if (checkList.Contains(blocks[row, column + 1])) return true;
		return false;
	}

	int countMatchUp(int row, int column)
	{
		matchUpCount = 0;

		countMatchUp_R(row, column);

		return matchUpCount;
	}
	int countMatchUp(int row, int column, BlockType type)
	{
		matchUpCount = 0;

		countMatchUp_R(row, column, type);

		return matchUpCount;
	}
	void countMatchUp_R(int row, int column)
	{
		if (row > 0)
		{
			if (blocks[row, column].type == blocks[row - 1, column].type && blocks[row - 1, column].item != ItemType.Ultimate)
			{
				matchUpCount++;
				countMatchUp_R(row - 1, column);
			}
		}
	}
	void countMatchUp_R(int row, int column, BlockType type)
	{
		if (row > 0)
		{
			if (type == blocks[row - 1, column].type && blocks[row - 1, column].item != ItemType.Ultimate)
			{
				matchUpCount++;
				countMatchUp_R(row - 1, column);
			}
		}
	}
	int matchUpCount;

	int countMatchDown(int row, int column)
	{
		matchDownCount = 0;

		countMatchDown_R(row, column);

		return matchDownCount;
	}
	int countMatchDown(int row, int column, BlockType type)
	{
		matchDownCount = 0;

		countMatchDown_R(row, column, type);

		return matchDownCount;
	}
	void countMatchDown_R(int row, int column)
	{
		if (row < this.rowSize - 1)
		{
			if (blocks[row, column].type == blocks[row + 1, column].type && blocks[row + 1, column].item != ItemType.Ultimate)
			{
				matchDownCount++;
				countMatchDown_R(row + 1, column);
			}
		}
	}
	void countMatchDown_R(int row, int column, BlockType type)
	{
		if (row < rowSize - 1)
		{
			if (type == blocks[row + 1, column].type && blocks[row + 1, column].item != ItemType.Ultimate)
			{
				matchDownCount++;
				countMatchDown_R(row + 1, column, type);
			}
		}
	}
	int matchDownCount;

	int countMatchLeft(int row, int column)
	{
		matchLeftCount = 0;

		countMatchLeft_R(row, column);

		return matchLeftCount;
	}
	int countMatchLeft(int row, int column, BlockType type)
	{
		matchLeftCount = 0;

		countMatchLeft_R(row, column, type);

		return matchLeftCount;
	}
	void countMatchLeft_R(int row, int column)
	{
		if (column > 0)
		{
			if (blocks[row, column].type == blocks[row, column - 1].type && blocks[row, column - 1].item != ItemType.Ultimate)
			{
				matchLeftCount++;
				countMatchLeft_R(row, column - 1);
			}
		}
	}
	void countMatchLeft_R(int row, int column, BlockType type)
	{
		if (column > 0)
		{
			if (type == blocks[row, column - 1].type && blocks[row, column - 1].item != ItemType.Ultimate)
			{
				matchLeftCount++;
				countMatchLeft_R(row, column - 1);
			}
		}
	}
	int matchLeftCount;

	int countMatchRight(int row, int column)
	{
		matchRightCount = 0;

		countMatchRight_R(row, column);

		return matchRightCount;
	}
	int countMatchRight(int row, int column, BlockType type)
	{
		matchRightCount = 0;

		countMatchRight_R(row, column, type);

		return matchRightCount;
	}
	void countMatchRight_R(int row, int column)
	{
		if (column < this.columnSize - 1)
		{
			if (blocks[row, column].type == blocks[row, column + 1].type && blocks[row, column + 1].item != ItemType.Ultimate)
			{
				matchRightCount++;
				countMatchRight_R(row, column + 1);
			}
		}
	}
	void countMatchRight_R(int row, int column, BlockType type)
	{
		if (column < columnSize - 1)
		{
			if (type == blocks[row, column + 1].type && blocks[row, column + 1].item != ItemType.Ultimate)
			{
				matchRightCount++;
				countMatchRight_R(row, column + 1);
			}
		}
	}
	int matchRightCount;

	bool checkMatchPossible()
	{
		for (int r = 0; r < rowSize; r++)
			for (int c = 0; c < columnSize; c++)
			{
				if (isMatchPossible(r, c, blocks[r, c].type)) return true;
			}
		return false;
	}
	bool isMatchPossible(int row, int column, BlockType type)
	{
		bool up = isMatchPossibleUp(row - 1, column, type);
		bool down = isMatchPossibleDown(row + 1, column, type);
		bool left = isMatchPossibleLeft(row, column - 1, type);
		bool right = isMatchPossibleRight(row, column + 1, type);

		if (up || down || left || right) return true;
		return false;
	}
	bool isMatchPossibleUp(int row, int column, BlockType type)
	{
		if (row >= 0)
		{
			int r = row;
			int c = column;
			int min = Constants.minimumMatch;

			int up = countMatchUp(r, c, type) + 1;

			int left = countMatchLeft(r, c, type);
			int right = countMatchRight(r, c, type);
			int lr = left + right + 1;
			
			if (up >= min || lr >= min) return true;
		}
		return false;
	}
	bool isMatchPossibleDown(int row, int column, BlockType type)
	{
		if (row < rowSize)
		{
			int r = row;
			int c = column;
			int min = Constants.minimumMatch;

			int down = countMatchDown(r, c, type) + 1;

			int left = countMatchLeft(r, c, type);
			int right = countMatchRight(r, c, type);
			int lr = left + right + 1;
			
			if (down >= min || lr >= min) return true;
		}
		
		return false;
	}
	bool isMatchPossibleLeft(int row, int column, BlockType type)
	{
		if (column >= 0)
		{
			int r = row;
			int c = column;
			int min = Constants.minimumMatch;

			int left = countMatchLeft(r, c, type) + 1;

			int up = countMatchUp(r, c, type);
			int down = countMatchDown(r, c, type);
			int ud = up + down + 1;
			
			if (left >= min || ud >= min) return true;
		}
		return false;
	}
	bool isMatchPossibleRight(int row, int column, BlockType type)
	{
		if (column < columnSize)
		{
			int r = row;
			int c = column;
			int min = Constants.minimumMatch;

			int right = countMatchRight(r, c, type) + 1;

			int up = countMatchUp(r, c, type);
			int down = countMatchDown(r, c, type);
			int ud = up + down + 1;

			if (right >= min || ud >= min) return true;
		}

		return false;
	}

	IEnumerator DestroyBlockListForBoth(Block itemBlock)
	{
		ContainerStop(true);

		List<Block> temp = GetBlocksVerticalAndHorizontal(itemBlock);
		for (int i = 0; i < temp.Count; i++) if (!matchList.Contains(temp[i])) matchList.Add(temp[i]);

		EffectItemHorizontal(itemBlock.index);// 이펙트 실행
		EffectItemVertical(itemBlock.index);

		DestroyBlockList(matchList);

		PlaySoundDestroyBoth();

		yield return new WaitForSeconds(Constants.destroyNormalDuration);

		ContainerStop(false);
	}
	IEnumerator DestroyBlockListForBoxAndBoth(Block itemBlock)
	{
		ContainerStop(true);

		List<Block> tempB = GetBlocksHorizontal(itemBlock, 3);
		for (int i = 0; i < tempB.Count; i++) if (!matchList.Contains(tempB[i])) matchList.Add(tempB[i]);
		DestroyBlockList(matchList);
		EffectItemHorizontal3(itemBlock.index);

		PlaySoundDestroyBoxAndBoth();

		yield return new WaitForSeconds(Constants.destroyNormalDuration);

		List<Block> tempA = GetBlocksVertical(itemBlock, 3);
		for (int i = 0; i < tempA.Count; i++) if (!matchList.Contains(tempA[i])) matchList.Add(tempA[i]);
		DestroyBlockList(matchList);
		EffectItemVertical3(itemBlock.index);

		PlaySoundDestroyBoxAndBoth();

		yield return new WaitForSeconds(Constants.destroyNormalDuration);

		ContainerStop(false);
	}
	IEnumerator DestroyBlockListForBoxAndBox(Block itemBlock)
	{
		List<Block> tempA = GetBlocksBox(itemBlock, 5);
		for (int i = 0; i < tempA.Count; i++) if (!matchList.Contains(tempA[i])) matchList.Add(tempA[i]);
		DestroyBlockList(matchList);
		EffectItemBoxAndBox(itemBlock.index);

		PlaySoundDestroyBoxAndBox();

		yield return new WaitForSeconds(Constants.destroyNormalDuration);
	}
	IEnumerator DestroyBlockListForUltimate(Block a, Block b)
	{
		Block anotherBlock;
		Block itemBlock;
		if (a.item == ItemType.Ultimate)
		{
			anotherBlock = b;
			itemBlock = a;
		}
		else
		{
			anotherBlock = a;
			itemBlock = b;
		}

		ContainerStop(true);

		List<Block> temp = GetBlocksUltimate(anotherBlock.type);

		for (int i = 0; i < temp.Count; i++) if (!matchList.Contains(temp[i])) if (temp[i].item != ItemType.Ultimate) matchList.Add(temp[i]);

		float tempT = matchList.Count * Constants.destroyUltimateItemTick + Constants.destroyNormalDuration;// 이펙트 시간 구하기
		EffectItemUltimate(itemBlock.index, tempT);// 이펙트 실행

		for (int i = 0; i < matchList.Count; i++)// ultimate 는 따로 Destroy
		{
			matchList[i].Destroy();

			EffectItemUltimateLine(itemBlock.index, matchList[i].index);// 하나하나 파괴하는 이펙트
			PlaySoundDestroyUltimate();

			yield return new WaitForSeconds(Constants.destroyUltimateItemTick);
		}

		yield return new WaitForSeconds(Constants.destroyNormalDuration);

		ContainerStop(false);
	}
	IEnumerator DestroyBlockListForUltimateAndBoth(Block a, Block b)
	{
		Block anotherBlock;
		Block itemBlock;
		if (a.item == ItemType.Ultimate)
		{
			anotherBlock = b;
			itemBlock = a;
		}
		else
		{
			anotherBlock = a;
			itemBlock = b;
		}

		ContainerStop(true);

		List<Block> temp = GetBlocksUltimate(anotherBlock.type);// ultimate 가 아닌 Block 의 type 으로 리스트 가져옴

		float tempT = temp.Count * Constants.destroyUltimateItemTick + Constants.destroyNormalDuration;// 이펙트 시간 구하기
		EffectItemUltimate(itemBlock.index, tempT);// 이펙트 실행

		for (int i = 0; i < temp.Count; i++)// 아이템으로 변경 및 매치리스트에 담기
		{
			if (!matchList.Contains(temp[i]) && temp[i].item == ItemType.None)
			{
				int r = Random.Range(0, 2);
				switch (r)
				{
					case 0: temp[i].SetItem(ItemType.Horizontal); break;
					case 1: temp[i].SetItem(ItemType.Vertical); break;
				}
				matchList.Add(temp[i]);

				EffectItemUltimateLine(itemBlock.index, temp[i].index);// 하나하나 파괴하는 이펙트
				PlaySoundDestroyUltimateAndBoth();

				yield return new WaitForSeconds(Constants.destroyUltimateItemTick);
			}
		}

		for (int i = 0; i < matchList.Count; i++)// 디스트로이
		{
			if (matchList[i].Destroy())
			{
				yield return new WaitForSeconds(Constants.destroyUltimateItemTick);
			}
		}

		yield return new WaitForSeconds(Constants.destroyNormalDuration);

		ContainerStop(false);
	}
	IEnumerator DestroyBlockListForUltimateAndBox(Block a, Block b)
	{
		Block anotherBlock;
		Block itemBlock;
		if (a.item == ItemType.Ultimate)
		{
			anotherBlock = b;
			itemBlock = a;
		}
		else
		{
			anotherBlock = a;
			itemBlock = b;
		}

		ContainerStop(true);

		List<Block> temp = GetBlocksUltimate(anotherBlock.type);// ultimate 가 아닌 Block 의 type 으로 리스트 가져옴

		float tempT = temp.Count * Constants.destroyUltimateItemTick + Constants.destroyNormalDuration;// 이펙트 시간 구하기
		EffectItemUltimate(itemBlock.index, tempT);// 이펙트 실행

		for (int i = 0; i < temp.Count; i++)// 아이템으로 변경 및 매치리스트에 담기
		{
			if (!matchList.Contains(temp[i]) && temp[i].item == ItemType.None)
			{
				temp[i].SetItem(ItemType.Box);
				matchList.Add(temp[i]);

				EffectItemUltimateLine(itemBlock.index, temp[i].index);// 하나하나 파괴하는 이펙트
				PlaySoundDestroyUltimateAndBox();

				yield return new WaitForSeconds(Constants.destroyUltimateItemTick);
			}
		}

		for (int i = 0; i < matchList.Count; i++)// 디스트로이
		{
			if (matchList[i].Destroy())
			{
				yield return new WaitForSeconds(Constants.destroyUltimateItemTick);
			}
		}

		yield return new WaitForSeconds(Constants.destroyNormalDuration);

		ContainerStop(false);
	}
	IEnumerator DestroyBlockListForUltimateAndUltimate(Block a, Block b)
	{
		ContainerStop(true);

		List<Block> temp = new List<Block>();
		foreach (Block block in blocks) temp.Add(block);

		float tempT = temp.Count * Constants.destroyUltimateItemTick + Constants.destroyNormalDuration;// 이펙트 시간 구하기
		EffectItemUltimate(a.index, tempT);// 이펙트 실행

		for (int i = 0; i < temp.Count; i++)// 디스트로이
		{
			if (temp[i].Destroy())
			{
				EffectItemUltimateLine(a.index, temp[i].index);// 하나하나 파괴하는 이펙트
				PlaySoundDestroyUltimateAndUltimate();

				yield return new WaitForSeconds(Constants.destroyUltimateItemTick);
			}
		}

		yield return new WaitForSeconds(Constants.destroyNormalDuration);

		ContainerStop(false);
	}

	IEnumerator ScoreChange_C(int preValue, int currentValue)
	{
		isScoreChange = true;
		float proc = 0f;
		while (true)
		{
			proc = proc + 0.04f;
			float resValue = Mathf.Lerp((float)preValue, (float)currentValue, proc);
			scoreLabel.text = Mathf.RoundToInt(resValue).ToString();
			if (proc >= 1f) break;
			yield return new WaitForSeconds(0.02f);
		}
		isScoreChange = false;
	}
	bool isScoreChange;

	#endregion

	#region Event Functions

	public void FinishedMoveEvent(Block block)
	{
		SetMatchList(block);
		moveCount--;
		
		if (moveCount == 0)
		{
			bool isDestroy = false; if (matchList.Count != 0) isDestroy = true;

			DestroyBlockList(matchList);

			if (isDestroy)
			{
				PlaySoundDestroyNormal();
			}
			else
			{
				if (!checkMatchPossible()) ResetAllBlockCannotMatch();
				isMatchDone = true;
			}
		}
	}
	[SerializeField]
	public int moveCount;

	#endregion

	#region Public Utils

	public Vector2 IndexToPosition(Index index)
	{
		Vector2 result = new Vector2((int)(sidePad + index.column * blockBeta), (int)(-(sidePad + index.row * blockBeta)));
		return result + revisePosition;
	}
	public Vector2 IndexToPosition(int row, int column)
	{
		Vector2 result = new Vector2((int)(sidePad + column * blockBeta), (int)(-(sidePad + row * blockBeta)));
		return result + revisePosition;
	}
	public Vector2 IndexRowToPosition(int row)
	{
		Vector2 result = new Vector2(0f, (float)(-(sidePad + row * blockBeta)));
		return result + new Vector2(0f, revisePosition.y);
	}
	public Vector2 IndexColumnToPosition(int column)
	{
		Vector2 result = new Vector2((float)(sidePad + column * blockBeta), mRect.rect.height * 0.5f);
		return result + new Vector2(revisePosition.x, 0f);
	}

	public bool CompareIndex(Index a, Index b)
	{
		if (a.row == b.row && a.column == b.column) return true;
		else return false;
	}

	#endregion

	#endregion
}
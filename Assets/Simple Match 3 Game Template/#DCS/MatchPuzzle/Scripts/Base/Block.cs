﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;// for UGUI Image Component
using System.Collections.Generic;// for List<>
using DCS.MatchPuzzle.Base;// for Enum and Constants and Structs

[RequireComponent(typeof(Image))]
public class Block : MonoBehaviour
{
	#region Custom Part

	/// sprite images
	public Sprite[] blockSprites;

	/// setting image function with 'ItemType' and 'BlockType'
	void SetImageProcess()
	{
		switch (_item)
		{
			case ItemType.None:
				{
					switch (_type)
					{
						case BlockType.Red: mImage.sprite = blockSprites[1];
							break;
						case BlockType.Green: mImage.sprite = blockSprites[5];
							break;
						case BlockType.Blue: mImage.sprite = blockSprites[9];
							break;
						case BlockType.Yellow: mImage.sprite = blockSprites[13];
							break;
						default:
							break;
					}
				}
				break;
			case ItemType.Horizontal:
				{
					switch (_type)
					{
						case BlockType.Red: mImage.sprite = blockSprites[2];
							break;
						case BlockType.Green: mImage.sprite = blockSprites[6];
							break;
						case BlockType.Blue: mImage.sprite = blockSprites[10];
							break;
						case BlockType.Yellow: mImage.sprite = blockSprites[14];
							break;
						default:
							break;
					}
				}
				break;
			case ItemType.Vertical:
				{
					switch (_type)
					{
						case BlockType.Red: mImage.sprite = blockSprites[3];
							break;
						case BlockType.Green: mImage.sprite = blockSprites[7];
							break;
						case BlockType.Blue: mImage.sprite = blockSprites[11];
							break;
						case BlockType.Yellow: mImage.sprite = blockSprites[15];
							break;
						default:
							break;
					}
				}
				break;
			case ItemType.Box:
				{
					switch (_type)
					{
						case BlockType.Red: mImage.sprite = blockSprites[4];
							break;
						case BlockType.Green: mImage.sprite = blockSprites[8];
							break;
						case BlockType.Blue: mImage.sprite = blockSprites[12];
							break;
						case BlockType.Yellow: mImage.sprite = blockSprites[16];
							break;
						default:
							break;
					}
				}
				break;
			case ItemType.Ultimate: mImage.sprite = blockSprites[0];
				break;
			default:
				break;
		}
	}

	/// <summary>
	/// 블럭이 파괴될 때 사라지는 방법 설정
	/// (현재 scale 이 1 에서 0으로 변함)
	/// 
	/// setting function of
	/// how to destroy block
	/// (in present. scale change from 1 to 0)
	/// </summary>
	void DestroyProcess()
	{
		LeanTween.scale(gameObject, Vector2.zero, Constants.destroyNormalDuration).setFrom(Vector2.one).setEase(LeanTweenType.easeInOutSine);

		switch (destroyType)// add match point
		{
			case DestroyType.Normal: { mContainer.AddScore(Constants.destroyScoreNormal); } break;
			case DestroyType.Horizontal: { mContainer.AddScore(Constants.destroyScoreVerticalAndHorizontal); } break;
			case DestroyType.Vertical: { mContainer.AddScore(Constants.destroyScoreVerticalAndHorizontal); } break;
			case DestroyType.Box: { mContainer.AddScore(Constants.destroyScoreBox); } break;
			case DestroyType.Ultimate: { mContainer.AddScore(Constants.destroyScoreUltimate); } break;
		}
	}

	/// <summary>
	/// 블럭이 파괴된 후 아이템으로 변경되는 방법 설정
	/// (현재 scale 이 0 에서 1로 변함)
	/// 
	/// setting function of
	/// if you need after destroy change ItemType
	/// </summary>
	void AfterDestroyChangeToItemBlockProcess()
	{
		LeanTween.scale(gameObject, Vector2.one, Constants.changeToItemBlockDuration).setFrom(Vector2.zero).setEase(LeanTweenType.easeInOutSine);
	}

	/// <summary>
	/// 블럭이 아래로 내려갈 때 이동하는 방법 설정
	/// 
	/// setting function of
	/// how to move blocks down
	/// </summary>
	IEnumerator MoveProcess(Vector2 startPos, Vector2 endPos)
	{
		LeanTween.moveLocal(gameObject, endPos, Constants.moveDuration).setFrom(startPos).setEase(LeanTweenType.easeOutBounce);
		yield return new WaitForSeconds(Constants.moveDuration);
	}

	/// <summary>
	/// 블럭이 생성될 때 방법 설정
	/// 
	/// setting function of
	/// how to blocks on
	/// </summary>
	IEnumerator OnAnimationProcess()
	{
		SetState(State.Action);
		LeanTween.scale(gameObject, Vector2.one, Constants.blocksOnDuration).setFrom(Vector2.zero).setEase(LeanTweenType.easeOutBack);
		yield return new WaitForSeconds(Constants.blocksOnDuration);
		transform.localScale = Vector2.one;
		SetState(State.Idle);
	}

	/// <summary>
	/// 블럭이 화면에서 사라질 때 방법 설정
	/// 
	/// setting function of
	/// how to blocks off
	/// </summary>
	IEnumerator OffAnimationProcess()
	{
		SetState(State.Action);
		LeanTween.scale(gameObject, Vector2.zero, Constants.blocksOnDuration).setFrom(Vector2.one).setEase(LeanTweenType.easeInOutSine);
		yield return new WaitForSeconds(Constants.blocksOnDuration);
		transform.localScale = Vector2.zero;
	}

	#endregion

	#region Algorithm Part

	#region Enums

	public enum State : int
	{
		Empty,// BlockObject 가 파괴 혹은 생성 전 등의 이유로 비어진 상태
		Idle,// BlockObject 의 드래그가 가능한 대기상태
		Move,// BlockObject 가 이동하는 상태
		Action,// BlockObject 가 액션을 취하는 상태 (파괴 혹은 스왑 중일 때)
	}

	#endregion

	#region Fields

	public Index index { get { return _index; } }
	Index _index;

	public State state { get { return _state; } }
	State _state = State.Empty;

	public BlockType type { get { return _type; } }
	BlockType _type = BlockType.Red;

	public ItemType item { get { return _item; } }
	ItemType _item = ItemType.None;

	public ItemType afterDestroyItem { get { return _afterDestroyItem; } }
	ItemType _afterDestroyItem = ItemType.None;

	public DestroyType destroyType { get { return _destroyType; } }
	DestroyType _destroyType = DestroyType.Normal;

	Image mImage;

	static MatchContainer mContainer;

	#endregion

	#region Default Method

	void Start()
	{
		if (!mContainer) mContainer = transform.parent.GetComponent<MatchContainer>();
		if (!mImage) mImage = GetComponent<Image>();
	}

	#endregion
	#region Public Method

	public void SetState(State state)
	{
		_state = state;
	}

	public void SetType(BlockType type)
	{
		_type = type;

		SetImageProcess();
	}

	public void SetItem(ItemType item)
	{
		_item = item;

		SetImageProcess();
	}

	public void SetAfterDestroyItem(ItemType destroyAfterItem)
	{
		_afterDestroyItem = destroyAfterItem;
	}

	public void SetDestroyType(DestroyType destroyType)
	{
		_destroyType = destroyType;
	}

	public void SetIndex(int row, int column)
	{
		_index.row = row;
		_index.column = column;
	}

	public void SetPosition(int row, int column)
	{
		transform.localPosition = mContainer.IndexToPosition(row, column);
		transform.localScale = Vector2.one;
	}

	public void SetIndexAndPosition(int row, int column)
	{
		SetIndex(row, column);
		transform.localPosition = mContainer.IndexToPosition(row, column);
	}

	public void SetRandomType()
	{
		int r = Random.Range(0, 4);
		switch (r)
		{
			case 0: SetType(BlockType.Red); break;
			case 1: SetType(BlockType.Green); break;
			case 2: SetType(BlockType.Blue); break;
			case 3: SetType(BlockType.Yellow); break;
		}
	}

	public void Move(Index index)
	{
		StartCoroutine(Move_C(index));
	}

	public bool Destroy()
	{
		if (state == State.Action || state == State.Empty) return false;

		if (item == ItemType.None) StartCoroutine(DestroyNormal_C());
		else StartCoroutine(DestroyItem_C());
		return true;
	}

	public void SetOn()
	{
		if (!gameObject.activeInHierarchy) gameObject.SetActive(true);
		StartCoroutine(OnAnimationProcess());
	}

	public void SetOff()
	{
		StartCoroutine(OffAnimationProcess());
	}

	#endregion
	#region Private Methods

	IEnumerator Move_C(Index index)
	{
		if (state == State.Move) yield break;
		SetState(State.Move);
		mContainer.moveCount++;

		Vector2 startPos = transform.localPosition;
		Vector2 endPos = mContainer.IndexToPosition(index);

		yield return StartCoroutine(MoveProcess(startPos, endPos));

		transform.localPosition = endPos;
		transform.localScale = Vector2.one;

		SetState(State.Idle);

		mContainer.FinishedMoveEvent(this);
	}
	IEnumerator DestroyNormal_C()
	{
		SetState(State.Action);
		
		DestroyProcess();

		yield return new WaitForSeconds(Constants.destroyNormalDuration);

		if (afterDestroyItem == ItemType.None)
		{
			SetState(State.Empty);
			SetItem(afterDestroyItem);
			SetDestroyType(DestroyType.Normal);
		}
		else
		{
			mContainer.PlaySoundChangeToItemBlock();

			SetState(State.Idle);
			SetItem(afterDestroyItem);
			SetAfterDestroyItem(ItemType.None);
			SetDestroyType(DestroyType.Normal);

			AfterDestroyChangeToItemBlockProcess();
			yield return new WaitForSeconds(Constants.changeToItemBlockDuration);
		}
	}
	IEnumerator DestroyItem_C()
	{
		SetState(State.Action);
		List<Block> anotherDestroyList = new List<Block>();

		switch (item)
		{
			case ItemType.Horizontal:
				{
					anotherDestroyList = mContainer.GetBlocksHorizontal(this);

					for (int i = 0; i < anotherDestroyList.Count; i++) anotherDestroyList[i].SetDestroyType(DestroyType.Horizontal);
					SetDestroyType(DestroyType.Horizontal);

					mContainer.EffectItemHorizontal(index);

					mContainer.DestroyBlockList(anotherDestroyList);
					DestroyProcess();

					mContainer.PlaySoundDestroyHorizontal();
					
					yield return new WaitForSeconds(Constants.destroyNormalDuration);
				} break;
			case ItemType.Vertical:
				{
					anotherDestroyList = mContainer.GetBlocksVertical(this);

					for (int i = 0; i < anotherDestroyList.Count; i++) anotherDestroyList[i].SetDestroyType(DestroyType.Vertical);
					SetDestroyType(DestroyType.Vertical);

					mContainer.EffectItemVertical(index);

					mContainer.DestroyBlockList(anotherDestroyList);
					DestroyProcess();

					mContainer.PlaySoundDestroyVertical();

					yield return new WaitForSeconds(Constants.destroyNormalDuration);
				} break;
			case ItemType.Box:
				{
					anotherDestroyList = mContainer.GetBlocksBox(this, 3);
					
					for (int i = 0; i < anotherDestroyList.Count; i++) anotherDestroyList[i].SetDestroyType(DestroyType.Box);
					SetDestroyType(DestroyType.Box);

					mContainer.EffectItemBox(index);

					mContainer.DestroyBlockList(anotherDestroyList);
					DestroyProcess();

					mContainer.PlaySoundDestroyBox();

					yield return new WaitForSeconds(Constants.destroyNormalDuration);
				} break;
			case ItemType.Ultimate:
				{
					mContainer.ContainerStop(true);

					anotherDestroyList = mContainer.GetBlocksUltimate(this.type);

					SetDestroyType(DestroyType.Ultimate);

					float effectDuration = anotherDestroyList.Count * Constants.destroyUltimateItemTick + Constants.destroyNormalDuration;
					mContainer.EffectItemUltimate(index, effectDuration);
					
					for (int i = 0; i < anotherDestroyList.Count; i++)
					{
						anotherDestroyList[i].Destroy();
						mContainer.EffectItemUltimateLine(this.index, anotherDestroyList[i].index);// destroy effect one by one
						mContainer.PlaySoundDestroyUltimate();
						yield return new WaitForSeconds(Constants.destroyUltimateItemTick);
					}
					DestroyProcess();

					yield return new WaitForSeconds(Constants.destroyNormalDuration);

					mContainer.ContainerStop(false);
				} break;
		}

		if (afterDestroyItem == ItemType.None)
		{
			SetState(State.Empty);
			SetItem(afterDestroyItem);
			SetDestroyType(DestroyType.Normal);
		}
		else
		{
			mContainer.PlaySoundChangeToItemBlock();

			SetState(State.Idle);
			SetItem(afterDestroyItem);
			SetAfterDestroyItem(ItemType.None);
			SetDestroyType(DestroyType.Normal);

			AfterDestroyChangeToItemBlockProcess();
			yield return new WaitForSeconds(Constants.changeToItemBlockDuration);
		}
	}

	#endregion

	#endregion
}
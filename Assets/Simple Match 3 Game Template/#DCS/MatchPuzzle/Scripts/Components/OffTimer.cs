﻿using UnityEngine;
using System.Collections;

public class OffTimer : MonoBehaviour {

	public float activeFalseTime;

	void OnEnable()
	{
		StartCoroutine(OnEnableProcess());
	}

	IEnumerator OnEnableProcess()
	{
		yield return new WaitForSeconds(activeFalseTime);

		gameObject.SetActive(false);
	}
}

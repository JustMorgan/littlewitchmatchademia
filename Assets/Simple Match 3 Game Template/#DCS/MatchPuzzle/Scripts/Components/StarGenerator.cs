﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(RectTransform))]
public class StarGenerator : MonoBehaviour {

	public GameObject starPrefab;
	public int poolSize;

	public float minimumSize;
	public float maximumSize;

	public float starChangeDuration;

	List<RectTransform> pool = new List<RectTransform>();
	RectTransform mRect;

	void Start()
	{
		if (starPrefab == null) return;

		mRect = GetComponent<RectTransform>();
		pool.Clear();
		float halfW = mRect.sizeDelta.x * 0.5f;
		float halfH = mRect.sizeDelta.y * 0.5f;

		for (int i = 0; i < poolSize; i++)
		{
			float rs = Random.Range(minimumSize, maximumSize);

			float rpx = Random.Range(-halfW, halfW);
			float rpy = Random.Range(-halfH, halfH);
			
			GameObject obj = Instantiate(starPrefab) as GameObject;
			obj.transform.SetParent(transform);
			obj.transform.localScale = new Vector3(rs, rs);
			obj.transform.localPosition = new Vector3(rpx, rpy);
			obj.transform.Rotate(0f, 0f, Random.Range(0f, 360f));
			obj.GetComponent<Image>().CrossFadeAlpha(Random.Range(0.1f, 1f), 0f, true);
			pool.Add(obj.GetComponent<RectTransform>());
		}

		StartCoroutine(StarProcess());
	}

	IEnumerator StarProcess()
	{
		while (true)
		{
			RectTransform rect = pool[Random.Range(0, poolSize)];

			if (!LeanTween.isTweening(rect.gameObject))
			{
				float ra = Random.Range(0f, 1f);
				float rd = Random.Range(0.3f, starChangeDuration);
				int loopCount = Random.Range(1, 3);

				LeanTween.alpha(rect, ra, rd).setEase(LeanTweenType.easeInOutSine).setLoopPingPong(loopCount);
				//yield return new WaitForSeconds(rd * loopCount);
			}	

			yield return new WaitForSeconds(0.3f);
		}
	}
}

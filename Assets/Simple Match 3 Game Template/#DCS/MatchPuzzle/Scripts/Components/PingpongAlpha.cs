﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class PingpongAlpha : MonoBehaviour {

	public float from, to;
	public float duration;
	public LeanTweenType easeType;

	void OnEnable()
	{
		ImageStarter();
	}

	void Awake()
	{
		ImageInit();
	}

	#region Image Component

	Image mImage;
	void ImageInit()
	{
		if (mImage == null) if (GetComponent<Image>()) mImage = GetComponent<Image>();
	}
	void ImageStarter()
	{
		if (mImage == null) return;
		StartCoroutine(ImageProcess());
	}
	IEnumerator ImageProcess()
	{
		while (true)
		{
			LeanTween.alpha(mImage.rectTransform, to, duration).setFrom(from).setEase(easeType);
			yield return new WaitForSeconds(duration);

			LeanTween.alpha(mImage.rectTransform, from, duration).setFrom(to).setEase(easeType);
			yield return new WaitForSeconds(duration);
		}
	}

	#endregion
}

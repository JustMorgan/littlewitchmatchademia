﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour
{
	/// How To Use
	/// 
	/// This component is a necessary component for managing all the sounds in the game in one of the script.
	/// Register as an AudioSource kind of sound redundant playable in the game, and all of the sounds in the game to be played register AudioClip, Public Function will be executed through a sound in MatchContainer.
	/// 
	/// [AudioSource] loopSource : Type sound AudioSource.
	/// [AudioClip[]] loopClips : Type sound AudipClip.
	/// [Function()] Play... : Type sound Play Function. It can be configured differently, if necessary.

	/// 사용 방법
	/// (Controller 를 직접 만들기 힘드신분은 참고하세여)
	/// 
	/// 이 컴포넌트는 게임내 필요한 모든 사운드를 한개의 스크립트에서 관리하기 위한 컴포넌트 입니다.
	/// 게임안에 중복플레이가 가능한 사운드의 종류만큼 AudioSource를 등록하고,
	/// 게임안에 플레이되어야 할 사운드를 모두 AudioClip으로 등록하여,
	/// Public 함수를 통해 MatchContainer 에서 사운드를 실행하게 됩니다.
	/// 
	/// 기본구조
	/// [AudioSource] loopSource : 종류별 사운드의 AudioSource
	/// [AudioClip[]] loopClips : 종류별 사운드의 AudioClip
	/// [Function()] Play... : 종류별 사운드의 Play 함수. 필요에 따라 다르게 셋팅할 수 있다.
	
	#region Loop

	public AudioSource loopSource;
	public AudioClip[] loopClips;
	void LoopSourceInit()
	{
		int temp = Random.Range(0, loopClips.Length);
		loopSource.clip = loopClips[temp];
		loopSource.loop = true;
		loopSource.Play();
		VolumeChange(loopSource, 0f, 0.5f, 4);
	}

	#endregion

	#region Beep

	public AudioSource beepSource;
	public AudioClip[] beepClips;

	public void PlayBeep()
	{
		int temp = Random.Range(0, beepClips.Length);
		beepSource.PlayOneShot(beepClips[temp]);
	}

	#endregion

	#region Beep Down

	public AudioSource beepDownSource;
	public AudioClip[] beepDownClips;

	public void PlayBeepDown()
	{
		int temp = Random.Range(0, beepDownClips.Length);
		beepDownSource.clip = beepDownClips[temp];
		beepDownSource.Play();
	}

	#endregion

	#region Crack

	public AudioSource crackSource;
	public AudioClip[] crackClips;

	public void PlayCrack()
	{
		int temp = Random.Range(0, crackClips.Length);
		crackSource.clip = crackClips[temp];
		crackSource.Play();
	}

	#endregion

	#region Dribble

	public AudioSource dribbleSource;
	public AudioClip[] dribbleClips;

	public void PlayDribble()
	{
		int temp = Random.Range(0, dribbleClips.Length);
		dribbleSource.clip = dribbleClips[temp];
		dribbleSource.Play();
	}

	#endregion

	#region Start Button

	public AudioSource startButtonSource;
	public AudioClip[] startButtonClips;

	public void PlayStartButton()
	{
		if (startButtonSource.isPlaying) return;
		int temp = Random.Range(0, startButtonClips.Length);
		startButtonSource.clip = startButtonClips[temp];
		startButtonSource.Play();
	}

	#endregion
	

	void Start()// Init
	{
		LoopSourceInit();
	}

	#region Utils

	void VolumeChange(AudioSource source, float to, float speed)
	{
		if (source != null) StartCoroutine(VolumeChange_C(source, to, speed));
	}
	IEnumerator VolumeChange_C(AudioSource source, float to, float speed)
	{
		float from = source.volume;
		float proc = 0f;
		while (true)
		{
			proc = proc + 0.04f;
			float resValue = Mathf.Lerp(from, to, proc);
			source.volume = resValue;
			if (proc >= 1f) break;
			yield return new WaitForSeconds(0.02f * speed);
		}
	}

	void VolumeChange(AudioSource source, float from, float to, float speed)
	{
		if (source != null) StartCoroutine(VolumeChange_C(source, from, to, speed));
	}
	IEnumerator VolumeChange_C(AudioSource source, float from, float to, float speed)
	{
		float proc = 0f;
		while (true)
		{
			proc = proc + 0.04f;
			float resValue = Mathf.Lerp(from, to, proc);
			source.volume = resValue;
			if (proc >= 1f) break;
			yield return new WaitForSeconds(0.02f * speed);
		}
	}

	void VolumeChange(AudioSource source, float to)
	{
		source.volume = to;
	}

	#endregion
}

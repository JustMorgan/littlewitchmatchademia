﻿using UnityEngine;
using System.Collections;

public class PingpongPosition : MonoBehaviour {

	public Vector2 from, to;
	public float duration;
	public LeanTweenType easeType;
	public bool isLocal = true;
	
	void OnEnable()
	{
		StartCoroutine(TweenProcess());
	}

	IEnumerator TweenProcess()
	{
		if (isLocal)
		{
			while (true)
			{
				LeanTween.moveLocal(gameObject, to, duration).setFrom(from).setEase(easeType);
				yield return new WaitForSeconds(duration);

				LeanTween.moveLocal(gameObject, from, duration).setFrom(to).setEase(easeType);
				yield return new WaitForSeconds(duration);
			}
		}
		else
		{
			while (true)
			{
				LeanTween.move(gameObject, to, duration).setFrom(from).setEase(easeType);
				yield return new WaitForSeconds(duration);

				LeanTween.move(gameObject, from, duration).setFrom(to).setEase(easeType);
				yield return new WaitForSeconds(duration);
			}
		}
	}

	#region Context Menus (if you need this functions. just Set [ExecuteInEditMode])

	[ContextMenu("Set From - Current Local Position")]
	public void SetFrom_CurrentLocalPosition()
	{
		from = transform.localPosition;
	}
	[ContextMenu("Set To - Current Local Position")]
	public void SetTo_CurrentLocalPosition()
	{
		to = transform.localPosition;
	}
	[ContextMenu("Set Local Positon - From")]
	public void SetLocalPosition_From()
	{
		transform.localPosition = from;
	}
	[ContextMenu("Set Local Position - To")]
	public void SetLocalPosition_To()
	{
		transform.localPosition = to;
	}

	#endregion
}

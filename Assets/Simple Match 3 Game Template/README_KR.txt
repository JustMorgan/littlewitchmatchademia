

<템플렛 실행 방법>

1. Simple Match 3 Game Template -> Scenes 폴더의 Start (Scene) 를 더블클릭 하십시오.
2. 상단의 플레이 버튼을 눌러 실행하십시오.
3. 게임이 실행된 후 Game 창에서 아무 곳이나 터치(클릭)하면 게임이 실행됩니다.
4. 기존의 Match3 형식 게임과 동일하게 플레이 하시면 됩니다.


<애셋 설명>

- Simple Match 3 Game Template 은 3 개의 Base Script 와 몇가지 Component 로 이루어져 있습니다.
- Base Script 의 폴더 경로는 '#DCS -> MatchPuzzle -> Scripts -> Base' 입니다.
- Base Script 의 코드는 Region 을 통해 'Custom Part' 와 'Algorythm Part' 로 나누어져있고, 'Custom Part' 를 수정하여 쉽게 커스터마이징 가능합니다. Algorythm Part 도 수정은 가능하지만 코드 구조 파악 후 수정하시는 것을 권장합니다.
- Components 의 폴더 경로는 '#DCS -> MatchPuzzle -> Scripts -> Components' 입니다.
- 중요한 Component Script 는 'EffectPool' 컴포넌트와 'SoundController' 컴포넌트가 있습니다. 그 외의 요소는 배경 오브젝트 컨트롤 용입니다. 애셋을 커스터마이징 할 경우 필요없다면 지우시는 것을 권장합니다.


<스크립트 설명 - Base>

/* Block 의 Custom Part
- blockSprites (Field) 를 통해 Block 을 표현할 sprite 리스트를 셋팅할 수 있습니다.
- SetImageProcess (Function) 를 통해 Block 의 ItemType, BlockType 별로 셋팅 가능합니다.
- DestroyProcess (Function) 를 통해 Block 의 파괴시 애니매이션 셋팅 가능합니다.
- AfterDestroyChangeToItemBlockProcess (Function) 를 통해 파괴 후 아이템으로 변경되어야 할 때 애니매이션 방식을 셋팅할 수 있습니다.
- MoveProcess (Coroutine Function) 를 통해 위에서 내려오는 애니매이션 셋팅이 가능합니다.
- OnAnimationProcess (Coroutine Function) 를 통해 Block 이 On 되었을 대 실행될 애니매이션 셋팅이 가능합니다.
- OffAnimationProcess (Coroutine Function) 를 통해 Block 이 Off 되었을 때 실행될 애니매이션 셋팅이 가능합니다.

/* MatchContainer 의 Custom Part
- rowSize (Field) 를 통해 가로줄 갯수를 셋팅할 수 있습니다.
- columnSize (Field) 를 통해 세로줄 갯수를 셋팅할 수 있습니다.
- blockPrefab (Field) 을 통해 Block 스크립트가 포함된 Prefab을 셋팅할 수 있습니다.
- blockSize (Field) 를 통해 Block 이 갖게될 영역을 셋팅할 수 있습니다. (단위 : pixel)
- blockSpacing (Field) 을 통해 Block 사이의 거리를 셋팅할 수 있습니다. (단위 : pixel)
- useItem (Field) 을 통해 아이템 사용 여부를 셋팅할 수 있습니다.
- scoreLabel (Field) 을 통해 점수 라벨을 셋팅할 수 있습니다.
- comboLabel (Field) 을 통해 콤보 라벨을 셋팅할 수 있습니다.
- Effect (Region) 안의 함수들로 각 상황별 이펙트를 셋팅할 수 있습니다.
- Sound (Region) 안의 함수들로 각 상황별 사운드를 셋팅할 수 있습니다.
- Combo (Region) 안의 함수로 콤보 라벨 표현 방식을 셋팅할 수 있습니다.

/* MatchPuzzleBase 의 Custom Part
- Enum BlockType 을 통해 Block 의 종류를 셋팅할 수 있습니다. 단. BlockType이 변화됨에 따라 Block 스크립트의 SetImageProcess를 수정해야 합니다. switch 문으로 셋팅되는 부분을 확장해서 사용하십시오.
- Constants 의 필드들을 통해 각종 애니매이션 관련 Duration 을 셋팅할 수 있습니다.
- Constants 의 필드들을 통해 파괴시 필요한 기본 점수들을 셋팅할 수 있습니다.
- Constants 의 필드들을 통해 콤보 체크에 필요한 수치들을 셋팅할 수 있습니다.


<스크립트 설명 - Components>

/* EffectPool 컴포넌트
- 'EffectPool' 컴포넌트는 Effect Pool 을 직접 만들기 귀찮거나, 힘드신 분들을 위한 샘플 입니다.
- Effect 를 심도있게 커스터마이징 하려면 Effect Pool 을 직접 제작하시는 것을 권장합니다.
- 자세한 사용 방법은 EffectPool 컴포넌트 스크립트 안에 주석으로 설명되어있습니다. 

/* SoundController 컴포넌트
- 'SoundController' 컴포넌트는 Sound 를 직접 컨트롤 하기 귀찮거나, 힘드신 분들을 위한 샘플 입니다.
- Sound 를 심도있게 커스터마이징 하려면 Sound Controller 를 직접 제작하시는 것을 권장합니다.
- 자세한 사용 방법은 SoundController 컴포넌트 스크립트 안에 주석으로 설명되어있습니다.

- 'OffTimer' 는 단순히 오브젝트가 OnEnable 되었을 때, 지정된 activeFalseTime 만큼 대기 후 오브젝트를 SetActive(false) 시켜주는 컴포넌트 입니다.
- 'PingPongAlpha' 는 단순히 오브젝트가 OnEnable 되었을 때, 지정된 from, to 값 만큼 Image 를 반복적으로 Alpha Tween 시켜주는 컴포넌트 입니다.
- 'PingPongPosition' 은 단순히 오브젝트가 OnEnable 되었을 때, 지정된 from, to 값 만큼 Position 을 반복적으로 Tween 시켜주는 컴포넌트 입니다.
- 'StarGenerator' 는 단순히 배경의 Star Object 들을 관리하기 위한 컴포넌트 입니다.
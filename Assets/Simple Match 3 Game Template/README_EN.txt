

<How To Use this template>

1. double click (Simple Match 3 Game Template -> Scenes) folder of 'Start'.
2. press the top of the 'Play' Button Run.
3. the game will run anywhere you touch (or click).
4. the same way you play and the traditional 'Match 3' type game.


<Detail - Asset>

- Simple Match 3 Game Template is made up of the 3 'Base Script' and some 'Components'.
- The folder path of 'Base Script' is '#DCS -> MatchPuzzle -> Scripts -> Base'.
- 'Base Script' of the code is divided into through a Region 'Custom Part' and 'Algorythm Part', by modifying the 'Custom Part' can be easily customized. 'Algorythm Part' modifications are also possible, but it is recommended that you modify the structure and code identification.
- The folder path of 'Components' is '#DCS -> MatchPuzzle -> Scripts -> Components'.
- Important Component Script is 'EffectPool' and 'SoundController'. The other elements is the control for background objects. It is recommended that you delete if you do not need to customize the asset.


<Script Detail - Base>

/* Block inside Custom Part
- blockSprites (Field) : You can set a sprite to represent the Block list with this script.
- SetImageProcess (Function) : You can set the 'Sprites' by 'BlockType' and 'ItemType'.
- DestroyProcess (Function) : You can set the 'Animation' of block destroy situation.
- AfterDestroyChangeToItemBlockProcess (Function) : You can set the 'How to change block by ItemBlock' after block destroy.
- MoveProcess (Coroutine Function) : You can set the 'How to move down by bottom is empty'.
- OnAnimationProcess (Coroutine Function) : You can set the 'How Blocks on'.
- OffAnimationProcess (Coroutine Function) : You can set the 'How Block off'.

/* MatchContainer inside Custom Part
- rowSize (Field) : You can set the number of horizontal lines.
- columnSize (Field) : You can set the number of vertical lines.
- blockPrefab (Field) : You can set the 'Block' script that contains 'Prefab'.
- blockSize (Field) : You can set the area to have this 'Block'. (Unit: pixel)
- blockSpacing (Field) : You can set the distance between Block. (Unit: pixel)
- useItem (Field) : Items you can set whether or not to use.
- scoreLabel (Field) : You can set the point label.
- comboLabel (Field) : You can set the combo label.
- Effect (Region) : You can set each function in this context with effect.
- Sound (Region) : You can set the sounds for each situation with a function within it.
- Combo (Region) : You can set the combo label expression as a function in it.

/* MatchPuzzleBase inside Custom Part
- BlockType (Enum) : You can set the type of Block. only. This turns 'Block Type', you must modify the 'Set Image Process' in 'Block Script'. Use to expand a section that is set by a switch statement.
- Through the 'Field' of Constants You can set various animation-related Duration.
- When destruction through the 'Field' of Constants You can set the necessary base score.
- Check-in combo with the 'Field' of Constants You can set the required value.


<Script Detail - Components>

/* EffectPool Component
- 'EffectPool' component 'Effect Pool' annoying or making a direct sample for tough people.
- To customize the depth effect is recommended that you create your own 'Effect Pool'.
- For detailed operation it is described as a comment in the 'EffectPool' component script.

/* SoundController Component
- 'Sound Controller' components are directly or cumbersome to control the sound, the sample for tough people.
- To customize the sound depth is recommended that you direct the production 'Sound Controller'.
- For detailed operation it is described as a comment in the 'SoundController' component script.

- 'OnTimer' are simply components that object 'OnEnable' when specified 'activeFalseTime' as the standby after the object 'SetActive (false)' to.
- 'PingPongAlpha' simply when an object is 'OnEnable', given 'from', 'to' value as 'Image' repeatedly 'Alpha Tween' is a component that was.
- 'PingPongPosition' simply when an object is 'OnEnable', given 'from', 'to' and 'Position' value as a component which repeatedly 'Tween' by.
- 'StarGenerator' is simply a component for managing the 'Star Object' in the background.